package org.jac.hib_test.test;


import org.jac.hib_test.entity.AceTestEntity;
import org.jac.hib_test.entity.UserEntity;
import org.jac.hibernate.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JAC.
 * Date: 14-3-25
 * Time: 下午1:14
 */
public class HbmTest {
    public static void main(String[] args) {

//        AceTestEntity acet =  new AceTestEntity();
//        acet.setName("test7");
//        acet.setValue("value7");
//        addAceTest(acet);
//        System.out.println("act"+acet.getId());
//        System.out.println("连接关闭，添加成功");
//        AceTestEntity aceTestEntity = getAceTest(1);
//        System.out.println("name:"+aceTestEntity.getName()+" value:"+aceTestEntity.getValue());
        int[] arr = new int[5];
        int t = arr[0];
        t = 1;
        System.out.println(arr[0]);

        List<String> list = new ArrayList<String>();
        list.add("ttt");
        String x = list.get(0);
        x = "ttt";

        System.out.println(list.get(0) == x);

        UserEntity u = new UserEntity();
        u.setId(1);
        List<UserEntity> uList = new ArrayList<UserEntity>();
        uList.add(u);
        UserEntity u2 = uList.get(0);
        u2.setId(10);


        System.out.println(u.getId());
        System.out.println(uList.get(0).getId());
        System.out.println(u2.getId());

    }

    static AceTestEntity getAceTest(int id){
        AceTestEntity aceTestEntity = null;
        Session session = null;
        try {
            session = HibernateUtil.getSession();
            aceTestEntity = (AceTestEntity) session.get(AceTestEntity.class, id);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return aceTestEntity;
    }

    static void addAceTest(AceTestEntity aceTestEntity){
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            session.save(aceTestEntity);
            transaction.commit();
        } catch (HibernateException e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null){
                session.close();
            }
        }
    }

    static void addAceTest1(AceTestEntity aceTestEntity){
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            session.save(aceTestEntity);
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
