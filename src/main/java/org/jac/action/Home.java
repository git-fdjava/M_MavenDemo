package org.jac.action;

import org.jac.hib_test.dao.UserDao;
import org.jac.hib_test.entity.UserEntity;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by JAC.
 * Date: 14-4-12
 * Time: 上午6:31
 */
public class Home extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doPost(request,response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.println("Hello Word For ACE_Test");

        writer.println("Test for MySQL Query1");
        writer.println("id  : name");
        List<UserEntity> ulist = (List<UserEntity>) UserDao.pagingList(1,20);
        for (Object o : ulist) {
            UserEntity user = (UserEntity) o;
            writer.println("id:"+user.getId()+" name:"+user.getName());
        }
        writer.close();
    }
}
